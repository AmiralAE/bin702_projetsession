import numpy as np 
import random 
from itertools import product
import pandas as pd
import csv

# on fixe le seed pour le processus aleatoire afin d'assurer la reproducibilite des resultats
np.random.seed(0)

#la valeur de (l,d) correspondant au probleme de motif plante a resoudre
l = 6
d = 2

#generer tous les motifs possibles a partir dun alphabet donne, dans ce cas pour ADN
sigma_alphabet = ['A','T','C','G']
dataset = []
motif_dict = []

for i in range(100):
    sequence = ''.join(random.choices(sigma_alphabet, weights=None, k=1000))
    dataset.append(sequence)

dataset = np.asarray(dataset)

for set in product(sigma_alphabet, repeat=l):
    motif_dict.append(''.join(set))
    print(''.join(set))

motif_dict = np.asarray(motif_dict)

print(motif_dict.shape)

#retourner la distance de Hamming entre deux sequences
def hamming_distance(s1, s2):
    #les deux séquences doivent avoir la meme longueur
    if len(s1) != len(s2):
        raise ValueError("Les deux sequences ont des longueures differentes")
    return sum(ch1 != ch2 for ch1, ch2 in zip(s1, s2))

#generer les partitions/clusters des d-voisins pour chaque motif, avec d distance de hamming
all_clusters_dict = {}
for motif1 in motif_dict:
    cluster_motif_d_neighbor = []
    for motif2 in motif_dict:
        if motif1 != motif2:
            distance_Hamming = hamming_distance(motif1, motif2)
            print(motif1, motif2, distance_Hamming)
            if distance_Hamming < d+1 :
                cluster_motif_d_neighbor.append(motif2)            
    all_clusters_dict[motif1] = cluster_motif_d_neighbor

# on sauvegarde cette base de donnees de clusters de motifs et leurs d-voisins sous format csv
w = csv.writer(open('database_cluster.csv', 'w'))
for key, val in all_clusters_dict.items():
    w.writerow([key,val])