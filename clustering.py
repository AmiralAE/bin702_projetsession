import numpy as np 
import random 
from itertools import product
import pandas as pd
import csv
import sklearn
import itertools
from itertools import product

# on fixe le seed pour le processus aleatoire afin d'assurer la reproducibilite des resultats
np.random.seed(0)

#lire le fichier des donnees des motifs et leurs partitions
cluster_database = pd.read_csv('database_cluster.csv', delimiter=',')
cluster_database = np.asarray(cluster_database)

#la valeur de (l,d) correspondant au probleme de motif plante a resoudre
l = 6
d = 2

#generer tous les motifs possibles a partir dun alphabet donne, dans ce cas pour ADN
sigma_alphabet = ['A','T','C','G']
dataset = []
motif_dict = []

#generer ensemble des sequences a partir d'un alphabet, k est la longueur de la séquence et le nombre des séquences est nombre_sequences
for i in range(1000):
    sequence = ''.join(random.choices(sigma_alphabet, weights=None, k=72))
    dataset.append(sequence)
dataset = np.asarray(dataset)

#retourner la distance de Hamming entre deux sequences
def hamming_distance(s1, s2):
    #les deux séquences doivent avoir la meme longueur
    if len(s1) != len(s2):
        raise ValueError("Les deux sequences ont des longueures differentes")
    return sum(ch1 != ch2 for ch1, ch2 in zip(s1, s2))

#generer ensemble des l-mers pour chaque sequence en entree
dataset_sustrings = []
for seq in dataset:
    L = []
    for i in range(0,len(seq)-l+1):
        L.append(seq[i:i+l])
    dataset_sustrings.append(L)
dataset_to_process = np.asarray(dataset_sustrings)
 
#generer tous les tuples possibles de l-mers par produit cartesien de n sequences avec n l-mers
group_all_combinations = itertools.product(*dataset_sustrings)
# pour chaque tuple, retourner l'intersection 
global_motif_found = []
for e in group_all_combinations:
    local_motif_found = []
    for k in range(len(cluster_database[:,1])):
        z = list((cluster_database[:,1][k]).split(','))
        z[0]=z[0][1:]
        z[-1]=z[-1][0:-1]
        z_list = []
        for x in z:
            x= x.replace("'", "")
            x= ''.join(x)
            if x[0]==' ':
                x = x[1:]
            if len(x)==6:
                z_list.append(x)        
        ret = list(set(e).intersection(set(z_list)))
        if len(ret) != 0:
            local_motif_found.append(cluster_database[:,0][k])
    if len(global_motif_found)==0:
        global_motif_found = local_motif_found
    else:
        global_motif_found = list(set(local_motif_found).intersection(set(global_motif_found)))

# retourner les motifs trouves dans les sequences et qui sont solutions du probleme de motif plante (6,2)
print('Les motifs finaux trouvés dans ensemble des sequences en entree est :', global_motif_found)