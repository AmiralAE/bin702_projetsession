## Structure du répertoire ##

Dans ce répertoire on a les fichiers suivant :

1. generateClusters.py : ce script permet de générer une base de données de clusters à partir d'un alphabet et une valeur d de la distance de Hamming 
2. clustering.py : ce script retourne l'ensemble des motifs solutions au problème de motif plante défini pour une valeur de (l, d), suivant une approche de clustering et recherche  
3. votecluster.py : script pour résoudre le problème de motif planté suivant une approche de vote 
4. qVoteCluster.py : script basé sur approche de vote, clustering et programmation dynamique 
 
---
## Pour tester les scripts

1. Cloner le répertoire dans l'espace de travail local, comme décrit en bas de ce fichier
2. Exécutez a commande suivante : ** python nom_fichier.py ** ,  avec nom_fichier est soit clustering, votecluster, qVoteCluster

3. Si vous voulez générer une autre base de données pour des valeurs différentes de l et d, Executez la commande suivante : ** python generateCluster.py ** 

N.B : On utilise python3.x avec x>5, donc python3.5/3.6/3.7, sont de bons choix. 
Python2 n'est plus à jour et adapté

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).