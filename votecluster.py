import numpy as np 
import random 
from itertools import product
import pandas as pd
import csv
import itertools
from itertools import product

# on fixe le seed pour le processus aleatoire afin d'assurer la reproducibilite des resultats
np.random.seed(42)

#lire le fichier des donnees des motifs et leurs partitions
cluster_database = pd.read_csv('database_cluster.csv')
cluster_database = np.asarray(cluster_database)

#la valeur de (l,d) correspondant au probleme de motif plante a resoudre
l = 6
d = 2


#generer tous les motifs possibles a partir dun alphabet donne, dans ce cas pour ADN
sigma_alphabet = ['A','T','C','G']
dataset = []
motif_dict = []
for set in product(sigma_alphabet, repeat=l):
    motif_dict.append(''.join(set))
motif_dict = np.asarray(motif_dict)

#generer ensemble des sequences a partir d'un alphabet, k est la longueur de la séquence et le nombre des séquences est nombre_sequences
nombre_sequences = 60
for i in range(nombre_sequences):
    sequence = ''.join(random.choices(sigma_alphabet, weights=None, k=120))
    dataset.append(sequence)

dataset = np.asarray(dataset)

#retourner la distance de Hamming entre deux sequences
def hamming_distance(s1, s2):
    #les deux séquences doivent avoir la meme longueur
    if len(s1) != len(s2):
        raise ValueError("Les deux sequences ont des longueures differentes")
    return sum(ch1 != ch2 for ch1, ch2 in zip(s1, s2))

#retourne le d-voisinage entre deux sequences pour lensemble des motifs communs a au plus d distance de hamming
def consistent_set(x,y):
    listF = list()
    for motif in motif_dict:
        d_x_motif = hamming_distance(motif,x)
        d_y_motif = hamming_distance(motif,y)
        if d_x_motif < d+1 and d_y_motif < d+1:
            listF.append(motif)
    return listF
    
#generer ensemble des l-mers pour chaque sequence en entree
dataset_sustrings = []
for seq in dataset:
    L = []
    m = len(seq)
    for i in range(0,len(seq)-l+1):
        L.append(seq[i:i+l])
    dataset_sustrings.append(L)
dataset_to_process = np.asarray(dataset_sustrings)

#initialiser deux tables de hachage
V = {}
R = {}
w = csv.writer(open('motif_found.csv', 'w'))

#fonction retournant l'ensemble des motifs suivant une strategie de vote et clustering
def VoteCluster(l, d, S):
    n = S.shape[0]
    for j in range(0,m-l+1):
        for motif in motif_dict:
            V[motif] = 1
            R[motif] = 1
        #returns the motif's position in the cluster database
        pos = np.argwhere(cluster_database[:,0]==S[0,j])
        #retrieves all the d-neighbors of the motif in position pos
        N_d_s1 = list((cluster_database[:,1][pos[0][0]]).split(','))
        N_d_s1[0] = N_d_s1[0][1:]
        N_d_s1[-1] = N_d_s1[-1][0:-1]
        for i in range(1,n):
            for t in range(0, m-l):
                out_list = consistent_set(S[0,j], S[i,t])
                if len(out_list) != 0:
                    for ele in out_list:
                        if R[ele] != i:
                            V[ele] += 1
                            R[ele] = i
        L_V = []
        for x in N_d_s1 :
            x = x.replace("'","")
            x = " ".join(x.split())
            print(x)
            if V[x] == n: 
                print('get in', n)
                L_V.append(x)
                print('This is the motif of interest:', x)
            else:
                print("This motif found in the input sequences")
        if len(L_V) != 0:
            w.writerow([S[0,j],L_V])
    return L_V

def main():
    VoteCluster(l=l, d=d, S=dataset_to_process)

if __name__ == "__main__":
    main()